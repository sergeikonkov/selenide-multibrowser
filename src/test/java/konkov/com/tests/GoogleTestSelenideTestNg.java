package konkov.com.tests;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;

import org.testng.annotations.Test;
import org.openqa.selenium.By;

/**
 * Created by Sergey Konkov on 1/12/2017.
 */
public class GoogleTestSelenideTestNg {

    @Test
    public void googleSelenideOpen() {
        open("https://google.com");
        $(".gsfi").sendKeys("Ooop, I did it again!");
    }
}
