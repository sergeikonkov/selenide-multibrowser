package konkov.com.listeners;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.IInvokedMethod;
import org.testng.IInvokedMethodListener2;
import org.testng.ITestContext;
import org.testng.ITestResult;

import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

/**
 * Created by Sergey Konkov on 1/12/2017.
 */
public class WebDriverListener implements IInvokedMethodListener2 {
    public void beforeInvocation(IInvokedMethod iInvokedMethod, ITestResult iTestResult, ITestContext iTestContext) {
        if (iInvokedMethod.isTestMethod()) {
            String browserName = iInvokedMethod.getTestMethod().getXmlTest().getLocalParameters().get("browser");
            System.out.println("!!! XmlListener: " + browserName);

            Configuration.browser = browserName;
        }
    }

    public void afterInvocation(IInvokedMethod iInvokedMethod, ITestResult iTestResult, ITestContext iTestContext) {
        if (iInvokedMethod.isTestMethod()) {
            WebDriver driver = getWebDriver();
            if(driver != null)
                getWebDriver().quit();
        }
    }

    public void beforeInvocation(IInvokedMethod iInvokedMethod, ITestResult iTestResult) {

    }

    public void afterInvocation(IInvokedMethod iInvokedMethod, ITestResult iTestResult) {

    }
}
